<?php

include_once __DIR__."/../model/Article.php";
date_default_timezone_set("Europe/Kiev");

$id = $_GET['id'];
$article = new Article();
$article->deleteById($id);

header('Location: ../index.php');
